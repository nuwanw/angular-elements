import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CodeName } from '../model/code-name';
import { SimpleService } from '../service/simple.service';

@Component({
  // selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {

  @Input() title;
  @Output() display = new EventEmitter();

  codeName: CodeName;

  constructor(private simpleService: SimpleService) {

    this.codeName = new CodeName();
    this.codeName.code = '123';
    this.simpleService.printMsgToConsole();

  }

  ngOnInit() {
  }

  showInfo(){
    this.display.emit('This is the result');
  }


}
