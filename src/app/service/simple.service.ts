/**
 * Copyright (c) 2016. CodeGen Ltd. - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Created by nuwanw
 * Date : 11/26/2019
 * Time : 1:54 PM
 */
import { Injectable } from '@angular/core';

@Injectable()
export class SimpleService {

  printMsgToConsole() {
    console.log(123);
  }

}

