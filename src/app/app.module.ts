import { Injector, NgModule } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { HelloWorldModule } from './hello-world/hello-world.module';

@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule,
    HelloWorldModule
  ],
  entryComponents: [HelloWorldComponent],
  providers: []
})
export class AppModule {
  constructor(injector: Injector) {
    const custom = createCustomElement(HelloWorldComponent, {injector: injector});
    customElements.define('angular-elements', custom);
  }

  ngDoBootstrap() {}

}
