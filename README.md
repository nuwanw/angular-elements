# Angular Elements

This project was created to get the first hands on experience in Angular elements

## Getting Started

It has one component 'hello-world' this is used to create the angular element. As you can see this is simple component which is having
simple input & output.

Also I have added simple service injection [simpleService: SimpleService] to the hello-world.component.ts as well.

## Changes to app.module.ts to make the hello-world component redder as elements

    const custom = createCustomElement(HelloWorldComponent, {injector: injector});
    customElements.define('angular-elements', custom);
    
    ngDoBootstrap() {}
    
It is required to install the angular elements and custom-elements dependencies

ng add @angular/elements

npm i @webcomponents/custom-elements --save

## building the angular element
I have created following script to build the hello world component and make one java script file to use

build-elements.js

for file concat I have used fs-extra dependency as below

npm install --save-dev concat fs-extra


## Running angular element
Run the bellow command

npm run build:elements

It creates copy the java script file to the "elements" folder the under the root directory.

I have used simple index.html file to include the angular element script.

Use bellow command to run the output

npx live-server elements

elements - directory which contains the pages that need to be served

<angular-elements title="Batman"></angular-elements>

<!-- import the element script -->
<script src="./hello-world-element.js"></script>

<!-- this is to handle the output events -->
<script>
  const component = document.querySelector('angular-elements');
  component.addEventListener('display', (event) => {
    alert(event.detail);
  });
</script> 


## Refer the below for more info

https://blog.bitsrc.io/using-angular-elements-why-and-how-part-1-35f7fd4f0457

https://medium.com/@IMM9O/web-components-with-angular-d0205c9db08f


