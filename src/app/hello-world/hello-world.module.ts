import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SimpleService } from '../service/simple.service';
import { HelloWorldComponent } from './hello-world.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    HelloWorldComponent
  ],
  declarations: [
    HelloWorldComponent
  ],
  providers: [SimpleService]
})
export class HelloWorldModule {
}
